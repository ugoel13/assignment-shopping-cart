/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scripts_cart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scripts/cart */ \"./src/scripts/cart.js\");\n/* harmony import */ var _scripts_cart__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scripts_cart__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _scripts_home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scripts/home */ \"./src/scripts/home.js\");\n/* harmony import */ var _scripts_home__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scripts_home__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _scripts_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scripts/login */ \"./src/scripts/login.js\");\n/* harmony import */ var _scripts_login__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_scripts_login__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _scripts_product__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scripts/product */ \"./src/scripts/product.js\");\n/* harmony import */ var _scripts_product__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_scripts_product__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _scripts_registration__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./scripts/registration */ \"./src/scripts/registration.js\");\n/* harmony import */ var _scripts_registration__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_scripts_registration__WEBPACK_IMPORTED_MODULE_4__);\n\n\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanM/YjYzNSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCIuL3NjcmlwdHMvY2FydFwiO1xuaW1wb3J0IFwiLi9zY3JpcHRzL2hvbWVcIjtcbmltcG9ydCBcIi4vc2NyaXB0cy9sb2dpblwiO1xuaW1wb3J0IFwiLi9zY3JpcHRzL3Byb2R1Y3RcIjtcbmltcG9ydCBcIi4vc2NyaXB0cy9yZWdpc3RyYXRpb25cIjtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/scripts/cart.js":
/*!*****************************!*\
  !*** ./src/scripts/cart.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function Cart(cart) {\n  this.items = cart.items || {};\n  this.totalItems = cart.totalItems || 0;\n  this.totalPrice = cart.totalPrice || 0;\n\n  this.add = function (item, id) {\n    var cartItem = this.items[id];\n\n    if (!cartItem) {\n      cartItem = this.items[id] = {\n        item: item,\n        quantity: 0,\n        price: 0\n      };\n    }\n\n    cartItem.quantity++;\n    cartItem.price = cartItem.item.price * cartItem.quantity;\n    this.totalItems++;\n    this.totalPrice += cartItem.item.price;\n  };\n\n  this.remove = function (id) {\n    this.totalItems -= this.items[id].quantity;\n    this.totalPrice -= this.items[id].price;\n    delete this.items[id];\n  };\n\n  this.getItems = function () {\n    var arr = [];\n\n    for (var id in this.items) {\n      arr.push(this.items[id]);\n    }\n\n    return arr;\n  };\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9jYXJ0LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvY2FydC5qcz80MDFjIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIENhcnQoY2FydCkge1xuICB0aGlzLml0ZW1zID0gY2FydC5pdGVtcyB8fCB7fTtcbiAgdGhpcy50b3RhbEl0ZW1zID0gY2FydC50b3RhbEl0ZW1zIHx8IDA7XG4gIHRoaXMudG90YWxQcmljZSA9IGNhcnQudG90YWxQcmljZSB8fCAwO1xuXG4gIHRoaXMuYWRkID0gZnVuY3Rpb24oaXRlbSwgaWQpIHtcbiAgICB2YXIgY2FydEl0ZW0gPSB0aGlzLml0ZW1zW2lkXTtcbiAgICBpZiAoIWNhcnRJdGVtKSB7XG4gICAgICBjYXJ0SXRlbSA9IHRoaXMuaXRlbXNbaWRdID0geyBpdGVtOiBpdGVtLCBxdWFudGl0eTogMCwgcHJpY2U6IDAgfTtcbiAgICB9XG4gICAgY2FydEl0ZW0ucXVhbnRpdHkrKztcbiAgICBjYXJ0SXRlbS5wcmljZSA9IGNhcnRJdGVtLml0ZW0ucHJpY2UgKiBjYXJ0SXRlbS5xdWFudGl0eTtcbiAgICB0aGlzLnRvdGFsSXRlbXMrKztcbiAgICB0aGlzLnRvdGFsUHJpY2UgKz0gY2FydEl0ZW0uaXRlbS5wcmljZTtcbiAgfTtcblxuICB0aGlzLnJlbW92ZSA9IGZ1bmN0aW9uKGlkKSB7XG4gICAgdGhpcy50b3RhbEl0ZW1zIC09IHRoaXMuaXRlbXNbaWRdLnF1YW50aXR5O1xuICAgIHRoaXMudG90YWxQcmljZSAtPSB0aGlzLml0ZW1zW2lkXS5wcmljZTtcbiAgICBkZWxldGUgdGhpcy5pdGVtc1tpZF07XG4gIH07XG5cbiAgdGhpcy5nZXRJdGVtcyA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBhcnIgPSBbXTtcbiAgICBmb3IgKHZhciBpZCBpbiB0aGlzLml0ZW1zKSB7XG4gICAgICBhcnIucHVzaCh0aGlzLml0ZW1zW2lkXSk7XG4gICAgfVxuICAgIHJldHVybiBhcnI7XG4gIH07XG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/scripts/cart.js\n");

/***/ }),

/***/ "./src/scripts/home.js":
/*!*****************************!*\
  !*** ./src/scripts/home.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var slideIndex = 1;\n\nvar showSlides = function showSlides(n) {\n  var i;\n  var slides = document.getElementsByClassName(\"slider\");\n  var dots = document.getElementsByClassName(\"dot\");\n\n  if (slides && dots) {\n    if (n > slides.length) {\n      slideIndex = 1;\n    }\n\n    if (n < 1) {\n      slideIndex = slides.length;\n    }\n\n    for (i = 0; i < slides.length; i++) {\n      slides[i].style.display = \"none\";\n    }\n\n    for (i = 0; i < dots.length; i++) {\n      dots[i].className = dots[i].className.replace(\" active\", \"\");\n    }\n\n    if (slides[slideIndex - 1]) slides[slideIndex - 1].style.display = \"flex\";\n    if (dots[slideIndex - 1]) dots[slideIndex - 1].className += \" active\";\n  }\n};\n\nvar leftArrow = document.getElementById(\"arrow-left\");\nvar rightArrow = document.getElementById(\"arrow-right\");\n\nvar changeSlide = function changeSlide(e) {\n  e.target.innerText.toUpperCase() === \"NEXT\" ? showSlides(slideIndex += 1) : showSlides(slideIndex += -1);\n};\n\nif (leftArrow) leftArrow.addEventListener(\"click\", changeSlide);\nif (rightArrow) rightArrow.addEventListener(\"click\", changeSlide);\n\nvar currentSlide = function currentSlide(n) {\n  showSlides(slideIndex = n + 1);\n};\n\nvar dots = document.querySelectorAll(\".dot\");\n\nif (dots) {\n  dots.forEach(function (element, i) {\n    element.addEventListener(\"click\", function () {\n      return currentSlide(i);\n    });\n    setTimeout(function () {\n      return currentSlide(i);\n    }, 1000);\n  });\n}\n\nshowSlides(slideIndex);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9ob21lLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvaG9tZS5qcz9kNjg3Il0sInNvdXJjZXNDb250ZW50IjpbImxldCBzbGlkZUluZGV4ID0gMTtcbmNvbnN0IHNob3dTbGlkZXMgPSBuID0+IHtcbiAgbGV0IGk7XG4gIGxldCBzbGlkZXMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwic2xpZGVyXCIpO1xuICBsZXQgZG90cyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJkb3RcIik7XG4gIGlmIChzbGlkZXMgJiYgZG90cykge1xuICAgIGlmIChuID4gc2xpZGVzLmxlbmd0aCkge1xuICAgICAgc2xpZGVJbmRleCA9IDE7XG4gICAgfVxuICAgIGlmIChuIDwgMSkge1xuICAgICAgc2xpZGVJbmRleCA9IHNsaWRlcy5sZW5ndGg7XG4gICAgfVxuICAgIGZvciAoaSA9IDA7IGkgPCBzbGlkZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHNsaWRlc1tpXS5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XG4gICAgfVxuICAgIGZvciAoaSA9IDA7IGkgPCBkb3RzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBkb3RzW2ldLmNsYXNzTmFtZSA9IGRvdHNbaV0uY2xhc3NOYW1lLnJlcGxhY2UoXCIgYWN0aXZlXCIsIFwiXCIpO1xuICAgIH1cbiAgICBpZiAoc2xpZGVzW3NsaWRlSW5kZXggLSAxXSkgc2xpZGVzW3NsaWRlSW5kZXggLSAxXS5zdHlsZS5kaXNwbGF5ID0gXCJmbGV4XCI7XG4gICAgaWYgKGRvdHNbc2xpZGVJbmRleCAtIDFdKSBkb3RzW3NsaWRlSW5kZXggLSAxXS5jbGFzc05hbWUgKz0gXCIgYWN0aXZlXCI7XG4gIH1cbn07XG5cbmxldCBsZWZ0QXJyb3cgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFycm93LWxlZnRcIik7XG5sZXQgcmlnaHRBcnJvdyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXJyb3ctcmlnaHRcIik7XG5jb25zdCBjaGFuZ2VTbGlkZSA9IGUgPT4ge1xuICBlLnRhcmdldC5pbm5lclRleHQudG9VcHBlckNhc2UoKSA9PT0gXCJORVhUXCJcbiAgICA/IHNob3dTbGlkZXMoKHNsaWRlSW5kZXggKz0gMSkpXG4gICAgOiBzaG93U2xpZGVzKChzbGlkZUluZGV4ICs9IC0xKSk7XG59O1xuaWYgKGxlZnRBcnJvdykgbGVmdEFycm93LmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBjaGFuZ2VTbGlkZSk7XG5pZiAocmlnaHRBcnJvdykgcmlnaHRBcnJvdy5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgY2hhbmdlU2xpZGUpO1xuY29uc3QgY3VycmVudFNsaWRlID0gbiA9PiB7XG4gIHNob3dTbGlkZXMoKHNsaWRlSW5kZXggPSBuICsgMSkpO1xufTtcblxubGV0IGRvdHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmRvdFwiKTtcbmlmIChkb3RzKSB7XG4gIGRvdHMuZm9yRWFjaCgoZWxlbWVudCwgaSkgPT4ge1xuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IGN1cnJlbnRTbGlkZShpKSk7XG4gICAgc2V0VGltZW91dCgoKSA9PiBjdXJyZW50U2xpZGUoaSksIDEwMDApO1xuICB9KTtcbn1cbnNob3dTbGlkZXMoc2xpZGVJbmRleCk7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/scripts/home.js\n");

/***/ }),

/***/ "./src/scripts/login.js":
/*!******************************!*\
  !*** ./src/scripts/login.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// add active class\nvar handleFocus = function handleFocus(e) {\n  var target = e.target;\n  target.parentNode.classList.add('active');\n}; // remove active class\n\n\nvar handleBlur = function handleBlur(e) {\n  var target = e.target;\n\n  if (!target.value) {\n    target.parentNode.classList.remove('active');\n  }\n}; // register events\n\n\nvar bindEvents = function bindEvents(element) {\n  var floatField = element.querySelector('input');\n  floatField.addEventListener('focus', handleFocus);\n  floatField.addEventListener('blur', handleBlur);\n}; // get DOM elements\n\n\nvar floatContainers = document.querySelectorAll('.inputContainer');\nfloatContainers.forEach(function (element) {\n  bindEvents(element);\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9sb2dpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL2xvZ2luLmpzPzFmMDYiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vLyBhZGQgYWN0aXZlIGNsYXNzXG5jb25zdCBoYW5kbGVGb2N1cyA9IChlKSA9PiB7XG4gICAgY29uc3QgdGFyZ2V0ID0gZS50YXJnZXQ7XG4gICAgdGFyZ2V0LnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG59O1xuLy8gcmVtb3ZlIGFjdGl2ZSBjbGFzc1xuY29uc3QgaGFuZGxlQmx1ciA9IChlKSA9PiB7XG4gICAgY29uc3QgdGFyZ2V0ID0gZS50YXJnZXQ7XG4gICAgaWYgKCF0YXJnZXQudmFsdWUpIHtcbiAgICAgICAgdGFyZ2V0LnBhcmVudE5vZGUuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XG4gICAgfVxufTtcbi8vIHJlZ2lzdGVyIGV2ZW50c1xuY29uc3QgYmluZEV2ZW50cyA9IChlbGVtZW50KSA9PiB7XG4gICAgY29uc3QgZmxvYXRGaWVsZCA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKTtcbiAgICBmbG9hdEZpZWxkLmFkZEV2ZW50TGlzdGVuZXIoJ2ZvY3VzJywgaGFuZGxlRm9jdXMpO1xuICAgIGZsb2F0RmllbGQuYWRkRXZlbnRMaXN0ZW5lcignYmx1cicsIGhhbmRsZUJsdXIpO1xufTtcblxuLy8gZ2V0IERPTSBlbGVtZW50c1xuXG5jb25zdCBmbG9hdENvbnRhaW5lcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuaW5wdXRDb250YWluZXInKTtcbmZsb2F0Q29udGFpbmVycy5mb3JFYWNoKChlbGVtZW50KSA9PiB7XG4gICAgYmluZEV2ZW50cyhlbGVtZW50KTtcbn0pO1xuXG4iXSwibWFwcGluZ3MiOiJBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/scripts/login.js\n");

/***/ }),

/***/ "./src/scripts/product.js":
/*!********************************!*\
  !*** ./src/scripts/product.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var filterSelection = function filterSelection(category) {\n  console.log(\"filterSelection is called\");\n  var productList, i;\n  productList = document.getElementsByClassName(\"product\");\n  if (category == \"all\") category = \"\";\n\n  if (productList) {\n    for (i = 0; i < productList.length; i++) {\n      if (productList[i].className.indexOf(category) > -1) {\n        productList[i].style.display = \"flex\";\n      } else {\n        productList[i].style.display = \"none\";\n      }\n    }\n  }\n};\n\nvar filterContainer = document.getElementById(\"filterContainer\");\n\nif (filterContainer) {\n  var filterLinks = filterContainer.getElementsByClassName(\"filtercategory\");\n\n  for (var i = 0; i < filterLinks.length; i++) {\n    filterLinks[i].addEventListener(\"click\", function () {\n      var current = document.getElementsByClassName(\"active\");\n      var tempActiveElem = 0;\n      tempActiveElem = this === current[0] ? 1 : 0;\n      current.length > 0 ? current[0].className = current[0].className.replace(\" active\", \"\") : null;\n      tempActiveElem === 0 ? this.className += \" active\" : filterSelection(\"all\");\n    });\n  }\n}\n\nif (document.getElementsByClassName(\"filter-category\")) {\n  var filterCategoryLinks = document.getElementsByClassName(\"filter-category\");\n\n  for (var _i = 0; _i < filterCategoryLinks.length; _i++) {\n    filterCategoryLinks[_i].addEventListener(\"click\", filterSelection);\n  }\n}\n\nvar filterSelectionDropdown = function filterSelectionDropdown() {\n  var x = document.getElementById(\"filterContainerDropdown\").value;\n  filterSelection(x);\n};\n\nfilterSelection(\"all\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9wcm9kdWN0LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvcHJvZHVjdC5qcz9hZDIwIl0sInNvdXJjZXNDb250ZW50IjpbImNvbnN0IGZpbHRlclNlbGVjdGlvbiA9IGNhdGVnb3J5ID0+IHtcbiAgY29uc29sZS5sb2coXCJmaWx0ZXJTZWxlY3Rpb24gaXMgY2FsbGVkXCIpO1xuICBsZXQgcHJvZHVjdExpc3QsIGk7XG4gIHByb2R1Y3RMaXN0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInByb2R1Y3RcIik7XG4gIGlmIChjYXRlZ29yeSA9PSBcImFsbFwiKSBjYXRlZ29yeSA9IFwiXCI7XG4gIGlmIChwcm9kdWN0TGlzdCkge1xuICAgIGZvciAoaSA9IDA7IGkgPCBwcm9kdWN0TGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKHByb2R1Y3RMaXN0W2ldLmNsYXNzTmFtZS5pbmRleE9mKGNhdGVnb3J5KSA+IC0xKSB7XG4gICAgICAgIHByb2R1Y3RMaXN0W2ldLnN0eWxlLmRpc3BsYXkgPSBcImZsZXhcIjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHByb2R1Y3RMaXN0W2ldLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn07XG5cbmxldCBmaWx0ZXJDb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZpbHRlckNvbnRhaW5lclwiKTtcbmlmIChmaWx0ZXJDb250YWluZXIpIHtcbiAgbGV0IGZpbHRlckxpbmtzID0gZmlsdGVyQ29udGFpbmVyLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJmaWx0ZXJjYXRlZ29yeVwiKTtcbiAgZm9yIChsZXQgaSA9IDA7IGkgPCBmaWx0ZXJMaW5rcy5sZW5ndGg7IGkrKykge1xuICAgIGZpbHRlckxpbmtzW2ldLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICAgIGxldCBjdXJyZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcImFjdGl2ZVwiKTtcbiAgICAgIGxldCB0ZW1wQWN0aXZlRWxlbSA9IDA7XG4gICAgICB0ZW1wQWN0aXZlRWxlbSA9IHRoaXMgPT09IGN1cnJlbnRbMF0gPyAxIDogMDtcbiAgICAgIGN1cnJlbnQubGVuZ3RoID4gMFxuICAgICAgICA/IChjdXJyZW50WzBdLmNsYXNzTmFtZSA9IGN1cnJlbnRbMF0uY2xhc3NOYW1lLnJlcGxhY2UoXCIgYWN0aXZlXCIsIFwiXCIpKVxuICAgICAgICA6IG51bGw7XG4gICAgICB0ZW1wQWN0aXZlRWxlbSA9PT0gMFxuICAgICAgICA/ICh0aGlzLmNsYXNzTmFtZSArPSBcIiBhY3RpdmVcIilcbiAgICAgICAgOiBmaWx0ZXJTZWxlY3Rpb24oXCJhbGxcIik7XG4gICAgfSk7XG4gIH1cbn1cblxuaWYgKGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJmaWx0ZXItY2F0ZWdvcnlcIikpIHtcbiAgY29uc3QgZmlsdGVyQ2F0ZWdvcnlMaW5rcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXG4gICAgXCJmaWx0ZXItY2F0ZWdvcnlcIlxuICApO1xuICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbHRlckNhdGVnb3J5TGlua3MubGVuZ3RoOyBpKyspIHtcbiAgICBmaWx0ZXJDYXRlZ29yeUxpbmtzW2ldLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmaWx0ZXJTZWxlY3Rpb24pO1xuICB9XG59XG5cbmNvbnN0IGZpbHRlclNlbGVjdGlvbkRyb3Bkb3duID0gKCkgPT4ge1xuICB2YXIgeCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZmlsdGVyQ29udGFpbmVyRHJvcGRvd25cIikudmFsdWU7XG4gIGZpbHRlclNlbGVjdGlvbih4KTtcbn07XG5maWx0ZXJTZWxlY3Rpb24oXCJhbGxcIik7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/scripts/product.js\n");

/***/ }),

/***/ "./src/scripts/registration.js":
/*!*************************************!*\
  !*** ./src/scripts/registration.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var cnfmPasswordField = document.getElementById(\"cnfmPswdContainer\");\nvar passwordField = document.getElementById(\"pswdContainer\");\n\nif (cnfmPasswordField && passwordField) {\n  var cnfmPasswordFieldText = cnfmPasswordField.innerText;\n  var passwordFieldText = passwordField.innerText;\n\n  var handleNavigation = function handleNavigation(e) {\n    if (cnfmPasswordFieldText !== passwordFieldText) {\n      alert(\"not matched\");\n    }\n  };\n\n  var submitBtn = document.getElementById(\"submitButton\");\n  if (submitBtn) submitBtn.addEventListener(\"click\", handleNavigation);\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9yZWdpc3RyYXRpb24uanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9yZWdpc3RyYXRpb24uanM/NTBlMyJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBjbmZtUGFzc3dvcmRGaWVsZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY25mbVBzd2RDb250YWluZXJcIik7XG5jb25zdCBwYXNzd29yZEZpZWxkID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwc3dkQ29udGFpbmVyXCIpO1xuaWYgKGNuZm1QYXNzd29yZEZpZWxkICYmIHBhc3N3b3JkRmllbGQpIHtcbiAgY29uc3QgY25mbVBhc3N3b3JkRmllbGRUZXh0ID0gY25mbVBhc3N3b3JkRmllbGQuaW5uZXJUZXh0O1xuICBjb25zdCBwYXNzd29yZEZpZWxkVGV4dCA9IHBhc3N3b3JkRmllbGQuaW5uZXJUZXh0O1xuICBjb25zdCBoYW5kbGVOYXZpZ2F0aW9uID0gZSA9PiB7XG4gICAgaWYgKGNuZm1QYXNzd29yZEZpZWxkVGV4dCAhPT0gcGFzc3dvcmRGaWVsZFRleHQpIHtcbiAgICAgIGFsZXJ0KFwibm90IG1hdGNoZWRcIik7XG4gICAgfVxuICB9O1xuICBjb25zdCBzdWJtaXRCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInN1Ym1pdEJ1dHRvblwiKTtcbiAgaWYgKHN1Ym1pdEJ0bikgc3VibWl0QnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBoYW5kbGVOYXZpZ2F0aW9uKTtcbn1cbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/scripts/registration.js\n");

/***/ })

/******/ });